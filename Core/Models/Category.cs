﻿using System.ComponentModel.DataAnnotations;

namespace Core.Models
{
    #pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public class Category : EntityBase
    {
        [StringLength(500)]
        [Required]
        public string Name { get; set; }
        [StringLength(1000)]
        public string? Description { get; set; }
        [StringLength(500)]
        public string? Icon { get; set; }
        public long ParentId { get; set; }
    }
}
