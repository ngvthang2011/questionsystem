﻿using Core.Common;
using System.ComponentModel.DataAnnotations;

namespace Core.Models
{
    public abstract class EntityBase
    {
        [Key]
        public long Id { get; set; }
        public Status Status { get; set; }
        [StringLength(500)]
        public string? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [StringLength(500)]
        public string? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
