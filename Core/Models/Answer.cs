﻿using System.ComponentModel.DataAnnotations;

namespace Core.Models
{
    #pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public class Answer : EntityBase
    {
        [StringLength(500)]
        [Required]
        public string Name { get; set; }
        [StringLength(1000)]
        public string? Description { get; set; }
        [StringLength(500)]
        public string? Hint { get; set; }
        [StringLength(500)]
        public string? Image { get; set; }
        public bool? IsCorrect { get; set; }
        public long QuestionId { get; set; }
    }
}
