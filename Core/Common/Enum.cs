﻿namespace Core.Common
{
    public enum Status : byte
    {
        InActive = 0,
        Active = 1,
        Deleted = 2
    }

    public enum QuestionType : byte
    {
        Single = 0,
        Multiple = 1
    }
}
