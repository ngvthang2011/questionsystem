﻿using Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Core.Database
{
    #pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public class DatabaseContext : DbContext
    {

        public DatabaseContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to mysql with connection string from app settings
            var connectionString = @"Server=localhost;Port=3306;Database=quizDb;Uid=root;Pwd=1234$;;MaximumPoolSize=500;";
            options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
        }

        public DbSet<Category> categories { get; set; }
        public DbSet<Answer> answers { get; set; }
        public DbSet<Question> questions { get; set; }
    }
}
