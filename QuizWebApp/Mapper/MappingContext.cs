﻿using AutoMapper;
using Core.Models;
using QuizWebApp.ViewModels;

namespace QuizWebApp.Mapper
{
    public class MappingContext : Profile
    {
        public MappingContext()
        {
            CreateMap<Category, CategoryViewModel>();
            CreateMap<CategoryViewModel, Category>();
            CreateMap<Question, QuestionViewModel>();
            CreateMap<QuestionViewModel, Question>();
            CreateMap<Answer, AnswerViewModel>();
            CreateMap<AnswerViewModel, Answer>();
        }
    }
}
