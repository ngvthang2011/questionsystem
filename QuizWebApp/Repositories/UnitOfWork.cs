﻿using Core.Database;
using Core.Models;

namespace QuizWebApp.Repositories
{
    public class UnitOfWork : IDisposable
    {
        public DatabaseContext DbContext { get; }

        public IRepository<Category> CategoryRepository { get; }
        public IRepository<Question> QuestionRepository { get; }
        public IRepository<Answer> AnswerRepository { get; }

        public UnitOfWork(
             DatabaseContext context,
             IRepository<Category> categoryRepository,
             IRepository<Question> questionRepository,
             IRepository<Answer> answerRepository)
        {
            DbContext = context;

            CategoryRepository = categoryRepository;
            CategoryRepository.DbContext = DbContext;

            QuestionRepository = questionRepository;
            QuestionRepository.DbContext = DbContext;

            AnswerRepository = answerRepository;
            AnswerRepository.DbContext = DbContext;
        }

        public int SaveChanges()
        {
            var iResult = DbContext.SaveChanges();
            return iResult;
        }

        public async Task<int> SaveChangesAsync()
        {
            var iResult = await DbContext.SaveChangesAsync();
            return iResult;
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}
