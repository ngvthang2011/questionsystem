﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace QuizWebApp.Repositories
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public class Repository<T> : IRepository<T> where T : class
    {
        public DbContext DbContext { get; set; }

        public Repository()
        {

        }

        public DbSet<T> DbSet
        {
            get
            {
                return DbContext.Set<T>();
            }
        }

        /// <summary>
        ///     Retrieve all data of repository
        /// </summary>
        /// <returns>IQueryable<T></returns>
        public IQueryable<T> GetAll()
        {
            return DbSet.AsNoTracking();
        }

        /// <summary>
        ///     Retrieve all data of repository by Condition
        /// </summary>
        /// <param name="expression">Condition</param>
        /// <returns>IQueryable<T></returns>
        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return DbSet.Where(expression).AsNoTracking();
        }

        /// <summary>
        ///     Find data by id
        /// </summary>
        /// <param name="id">Table's primary key</param>
        /// <returns>Object</returns>
        public T FindOrFail(object id)
        {
            try
            {
                var model = DbSet.Find(id);
                return model!;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///     Find data by id
        /// </summary>
        /// <param name="id">Table's primary key</param>
        /// <returns>Object</returns>
        public T? FindOrFailByCondition(Expression<Func<T, bool>> expression)
        {
            try
            {
                var model = DbSet.FirstOrDefault(expression);
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///     Save a new entity in repository
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns>Object</returns>
        public T Create(T entity)
        {
            try
            {
                var model = DbSet.Add(entity);
                return model.Entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///     Update a entity in repository by entity
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns>T</returns>
        public T UpdateByEntity(T entity)
        {
            try
            {
                DbSet.Update(entity);
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///     Update list entity in repository by entity
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns>T</returns>
        public bool UpdateByEntityList(IEnumerable<T> entityList)
        {
            try
            {
                foreach (var entity in entityList)
                {
                    DbSet.Update(entity);
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///    Delete a entity in repository by entity
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns>Bool</returns>
        public bool DeleteByEntity(T entity)
        {
            try
            {
                DbSet.Remove(entity);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <param name="orderBy"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public (List<T>, int) ReadAllLimit(int skip, int take, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, Expression<Func<T, bool>> filter)
        {
            try
            {
                var all = DbContext.Set<T>();
                IQueryable<T> relevant = all.AsQueryable();
                int total = 0;
                if (filter != null)
                {
                    total = all.Where(filter).Count();
                    relevant = all.Where(filter).Skip(skip).Take(take);
                }
                else
                {
                    total = all.Count();
                    relevant = all.Skip(skip).Take(take);
                }
                if (orderBy != null)
                {
                    return (orderBy(relevant).ToList(), total);
                }
                (List<T>, int) result = (relevant.ToList(), total);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
