﻿using AutoMapper;
using Core.Common;
using Core.Models;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using OfficeOpenXml;
using QuizWebApp.Repositories;
using QuizWebApp.ViewModels;
using System.Data;
using System.Reflection;
using Dapper;

namespace QuizWebApp.Services
{
#pragma warning disable CS8601 // Possible null reference assignment.
#pragma warning disable CS8602 // Dereference of a possibly null reference.
    public class QuizService : IQuizService
    {
        protected readonly IMapper _mapper;
        private readonly UnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;
        public QuizService(IMapper mapper, IConfiguration config, IWebHostEnvironment env, UnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _env = env;
            _unitOfWork = unitOfWork;
            _config = config;
        }

        public IEnumerable<CategoryViewModel> GetCategoryList()
        {
            try
            {
                var categoryList = _unitOfWork.CategoryRepository.GetAll();

                var categoryDtoList = from category in categoryList
                                      select new CategoryViewModel
                                      {
                                          Id = category.Id,
                                          Status = category.Status,
                                          Name = category.Name,
                                          Description = category.Description,
                                          Icon = category.Icon,
                                          ParentId = category.ParentId
                                      };

                return categoryDtoList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public CategoryViewModel? GetCategoryById(long id)
        {
            try
            {
                var category = _unitOfWork.CategoryRepository.FindOrFail(id);
                CategoryViewModel? categoryDto = null;

                if (category != null)
                {
                    categoryDto = _mapper.Map<CategoryViewModel>(category);
                }

                return categoryDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Category CreateUpdateCategory(CategoryViewModel categoryViewModel, string userName)
        {
            try
            {
                var category = _mapper.Map<Category>(categoryViewModel);

                if (categoryViewModel.Id != 0)
                {
                    category.ModifiedBy = userName;
                    category.ModifiedDate = DateTime.UtcNow;
                    category = _unitOfWork.CategoryRepository.UpdateByEntity(category);
                }
                else
                {
                    category.CreatedBy = userName;
                    category.CreatedDate = DateTime.UtcNow;
                    category = _unitOfWork.CategoryRepository.Create(category);
                }
                _unitOfWork.SaveChanges();
                return category;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult DeleteCategory(long id)
        {
            try
            {
                var stringSql = $"CALL DeleteCategory({id});";
                var connectionString = _config["ConnectionStrings:WebAppDatabase"];
                int result = 0;

                using (var dbConn = new MySqlConnection(connectionString))
                {
                    result = dbConn.Execute(stringSql);

                    if (result > 0)
                    {
                        return new JsonResult("Success");
                    }
                }

                return new JsonResult("Fail");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<QuestionViewModel> GetQuestionList()
        {
            try
            {
                var questionList = _unitOfWork.QuestionRepository.GetAll();
                var categoryList = _unitOfWork.CategoryRepository.GetAll();
                var questionDtoList = from question in questionList
                                      join category in categoryList
                                      on question.CategoryId equals category.Id
                                      select new QuestionViewModel
                                      {
                                          Id = question.Id,
                                          Status = question.Status,
                                          Name = question.Name,
                                          Description = question.Description,
                                          Image = question.Image,
                                          Level = question.Level,
                                          Type = question.Type,
                                          CategoryId = question.CategoryId,
                                          CategoryName = category.Name
                                      };

                return questionDtoList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public QuestionViewModel? GetQuestionById(long id)
        {
            try
            {
                var question = _unitOfWork.QuestionRepository.FindOrFail(id);
                QuestionViewModel? questionDto = null;

                if (question != null)
                {
                    questionDto = _mapper.Map<QuestionViewModel>(question);
                }

                return questionDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Question CreateUpdateQuestion(QuestionViewModel questionViewModel, string userName, IFormFile? imageFile)
        {
            try
            {
                var question = _mapper.Map<Question>(questionViewModel);

                if (imageFile != null)
                {
                    var fileName = $"{DateTime.UtcNow.Ticks}_{imageFile.FileName}";
                    var path = Path.Combine(_env.ContentRootPath, "wwwroot", "images", fileName);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        imageFile.CopyTo(stream);
                    }
                    question.Image = fileName;
                }

                if (questionViewModel.Id != 0)
                {
                    question.ModifiedBy = userName;
                    question.ModifiedDate = DateTime.UtcNow;
                    question = _unitOfWork.QuestionRepository.UpdateByEntity(question);
                }
                else
                {
                    question.CreatedBy = userName;
                    question.CreatedDate = DateTime.UtcNow;
                    question = _unitOfWork.QuestionRepository.Create(question);
                }
                _unitOfWork.SaveChanges();
                return question;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult DeleteQuestion(long id)
        {
            try
            {
                var question = _unitOfWork.QuestionRepository.FindOrFail(id);
                if (question != null)
                {
                    if (!string.IsNullOrEmpty(question.Image))
                    {
                        var fullPath = Path.Combine(_env.ContentRootPath, "wwwroot", "images", question.Image);
                        if (File.Exists(fullPath))
                        {
                            File.Delete(fullPath);
                        }
                    }
                    var result = _unitOfWork.QuestionRepository.DeleteByEntity(question);
                    if (result)
                    {
                        _unitOfWork.SaveChanges();
                        return new JsonResult("Success");
                    }

                    return new JsonResult("Fail");
                }

                return new JsonResult("Fail");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ImportExcelQuestion(IFormFile ExcelQuestionFile, string UserLogin)
        {
            try
            {
                using (var stream = new MemoryStream())
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelQuestionFile.CopyTo(stream);
                    using (var package = new ExcelPackage(stream))
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                        var rowCount = worksheet.Dimension.Rows;
                        var dateTimeNow = DateTime.UtcNow;
                        for (int row = 2; row <= rowCount; row++)
                        {

                            _unitOfWork.QuestionRepository.Create(new Question
                            {
                                Status = (Status)Enum.Parse(typeof(Status), worksheet.Cells[row, 2].Value.ToString().Trim()),
                                Name = worksheet.Cells[row, 3].Value.ToString().Trim(),
                                Description = worksheet.Cells[row, 4].Value.ToString().Trim(),
                                Level = byte.Parse(worksheet.Cells[row, 6].Value.ToString().Trim()),
                                Type = (QuestionType)Enum.Parse(typeof(QuestionType), worksheet.Cells[row, 7].Value.ToString().Trim()),
                                CategoryId = long.Parse(worksheet.Cells[row, 8].Value.ToString().Trim()),
                                CreatedDate = dateTimeNow,
                                CreatedBy = UserLogin
                            });
                        }

                        _unitOfWork.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<AnswerViewModel> GetAnswerList()
        {
            try
            {
                var answerList = _unitOfWork.AnswerRepository.GetAll();
                var answerDtoList = from answer in answerList
                                    select new AnswerViewModel
                                    {
                                        Id = answer.Id,
                                        Name = answer.Name,
                                        Description = answer.Description,
                                        Status = answer.Status,
                                        QuestionId = answer.QuestionId,
                                        Image = answer.Image,
                                        CreatedBy = answer.CreatedBy,
                                        CreatedDate = answer.CreatedDate
                                    };

                return answerDtoList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public AnswerViewModel? GetAnswerById(long id)
        {
            try
            {
                var answer = _unitOfWork.AnswerRepository.FindOrFail(id);
                AnswerViewModel? answerDto = null;

                if (answer != null)
                {
                    answerDto = _mapper.Map<AnswerViewModel>(answer);
                }

                return answerDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Answer CreateUpdateAnswer(AnswerViewModel answerViewModel, string userName, IFormFile? imageFile)
        {
            try
            {
                var answer = _mapper.Map<Answer>(answerViewModel);

                if (imageFile != null)
                {
                    var fileName = $"{DateTime.UtcNow.Ticks}_{imageFile.FileName}";
                    var path = Path.Combine(_env.ContentRootPath, "wwwroot", "images", fileName);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        imageFile.CopyTo(stream);
                    }
                    answer.Image = fileName;
                }

                if (answerViewModel.Id != 0)
                {
                    answer.ModifiedBy = userName;
                    answer.ModifiedDate = DateTime.UtcNow;
                    answer = _unitOfWork.AnswerRepository.UpdateByEntity(answer);
                }
                else
                {
                    answer.CreatedBy = userName;
                    answer.CreatedDate = DateTime.UtcNow;
                    answer = _unitOfWork.AnswerRepository.Create(answer);
                }
                _unitOfWork.SaveChanges();
                return answer;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult DeleteAnswer(long id)
        {
            try
            {
                var answer = _unitOfWork.AnswerRepository.FindOrFail(id);
                if (answer != null)
                {
                    if (!string.IsNullOrEmpty(answer.Image))
                    {
                        var fullPath = Path.Combine(_env.ContentRootPath, "wwwroot", "images", answer.Image);
                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.File.Delete(fullPath);
                        }
                    }
                    var result = _unitOfWork.AnswerRepository.DeleteByEntity(answer);
                    if (result)
                    {
                        _unitOfWork.SaveChanges();
                        return new JsonResult("Success");
                    }

                    return new JsonResult("Fail");
                }

                return new JsonResult("Fail");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ImportExcelAnswer(IFormFile ExcelAnswerFile, string UserLogin)
        {
            try
            {
                using (var stream = new MemoryStream())
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelAnswerFile.CopyTo(stream);
                    using (var package = new ExcelPackage(stream))
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                        var rowCount = worksheet.Dimension.Rows;
                        var dateTimeNow = DateTime.UtcNow;
                        for (int row = 2; row <= rowCount; row++)
                        {
                            _unitOfWork.AnswerRepository.Create(new Answer
                            {
                                Name = worksheet.Cells[row, 2].Value.ToString().Trim(),
                                Description = worksheet.Cells[row, 3].Value.ToString().Trim(),
                                Status = (Status)Enum.Parse(typeof(Status), worksheet.Cells[row, 4].Value.ToString().Trim()),
                                QuestionId = long.Parse(worksheet.Cells[row, 5].Value.ToString().Trim()),
                                CreatedDate = dateTimeNow,
                                CreatedBy = UserLogin
                            });
                        }

                        _unitOfWork.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        
    }
}
