using AutoMapper;
using Core.Database;
using Core.Models;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using QuizWebApp.Mapper;
using QuizWebApp.Providers;
using QuizWebApp.Repositories;
using QuizWebApp.Services;
using System.Globalization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

// Auto Mapper Configurations
var mapperConfig = new MapperConfiguration(mapperConfig =>
{
    mapperConfig.AddProfile(new MappingContext());
});

IMapper mapper = mapperConfig.CreateMapper();
builder.Services.AddSingleton(mapper);

// LoggerManager
builder.Services.AddSingleton<ILoggerManager, LoggerManager>();

builder.Services.AddTransient<IRepository<Category>, Repository<Category>>();
builder.Services.AddTransient<IRepository<Question>, Repository<Question>>();
builder.Services.AddTransient<IRepository<Answer>, Repository<Answer>>();
builder.Services.AddTransient<UnitOfWork>();
builder.Services.AddTransient<DatabaseContext>();

builder.Services.AddTransient<IQuizService, QuizService>();

builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");
builder.Services.AddMvc().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix).AddDataAnnotationsLocalization();

builder.Services.Configure<RequestLocalizationOptions>(
    otp =>
    {
        var supportedCultures = new List<CultureInfo>
        {
            new CultureInfo("en"),
            new CultureInfo("ja"),
            new CultureInfo("vi")
        };
        otp.DefaultRequestCulture = new RequestCulture("en");
        otp.SupportedCultures = supportedCultures;
        otp.SupportedUICultures = supportedCultures;
    });


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

var supportedCultures = new[] { "en", "ja", "vi" };
var culture = supportedCultures[0];

var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(culture)
    .AddSupportedCultures(supportedCultures)
    .AddSupportedUICultures(supportedCultures);

app.UseRequestLocalization(localizationOptions);

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    "category",
    "category",
    new { controller = "Home", action = "Index" });

app.MapControllerRoute(
    "createCategory",
    "category/create",
    new { controller = "Category", action = "CreateEditCategory" });

app.MapControllerRoute(
    "updateCategory",
    "category/update/{id:long}",
    new { controller = "Category", action = "CreateEditCategory" });

app.MapControllerRoute(
    "question",
    "question",
    new { controller = "Question", action = "Index" });

app.MapControllerRoute(
    "createQuestion",
    "question/create",
    new { controller = "Question", action = "CreateEditQuestion" });

app.MapControllerRoute(
    "updateQuestion",
    "question/update/{id:long}",
    new { controller = "Question", action = "CreateEditQuestion" });

app.MapControllerRoute(
    "answer",
    "answer",
    new { controller = "Answer", action = "Index" });

app.MapControllerRoute(
    "createAnswer",
    "answer/create",
    new { controller = "Answer", action = "CreateEditAnswer" });

app.MapControllerRoute(
    "updateQuestionAnswer",
    "answer/update/{id:long}",
    new { controller = "Answer", action = "CreateEditAnswer" });

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
