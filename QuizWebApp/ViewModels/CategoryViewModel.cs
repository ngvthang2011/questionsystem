﻿using Core.Common;
using System.ComponentModel.DataAnnotations;

namespace QuizWebApp.ViewModels
{
    public class CategoryViewModel
    {
        public long Id { get; set; }
        [Required]
        public Status Status { get; set; }
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Description { get; set; }
        public string? Icon { get; set; }
        public long ParentId { get; set; }
    }
}
