﻿using Core.Common;
using System.ComponentModel.DataAnnotations;

namespace QuizWebApp.ViewModels
{
    public class QuestionViewModel
    {
        public long Id { get; set; }
        [Required]
        public Status Status { get; set; }
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Description { get; set; }
        public string? Image { get; set; }
        [Required]
        public byte Level { get; set; }
        [Required]
        public QuestionType Type { get; set; }
        [Required]
        public long CategoryId { get; set; }
        public string? CategoryName { get; set; }
    }
}
