﻿using Core.Common;

namespace QuizWebApp.ViewModels
{
    public class AnswerViewModel
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public Status Status { get; set; }
        public long QuestionId { get; set; }
        public string? Image { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
