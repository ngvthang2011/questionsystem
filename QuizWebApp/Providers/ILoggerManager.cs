﻿namespace QuizWebApp.Providers
{
    public interface ILoggerManager
    {
        void LogInformation(string message);
        void LogError(string message);
    }
}
