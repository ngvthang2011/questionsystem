﻿using Microsoft.AspNetCore.Mvc;
using QuizWebApp.Providers;
using QuizWebApp.Services;
using QuizWebApp.ViewModels;

namespace QuizWebApp.Controllers
{
    #pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
    public class CategoryController : BaseController
    {
        private readonly ILoggerManager _loggerManager;
        private readonly QuizService _quizService;
        public CategoryController(ILoggerManager logger, IQuizService quizService)
        {
            _loggerManager = logger;
            _quizService = (QuizService)quizService;
        }

        public IActionResult CreateEditCategory(long? id)
        {
            try
            {
                CategoryViewModel category;

                if (id != null && id != 0)
                {
                    ViewData["Title"] = "Update Category";
                    category = _quizService.GetCategoryById((long)id);
                }
                else
                {
                    ViewData["Title"] = "Create New Category";
                    category = new CategoryViewModel();
                }

                return View(category);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult CreateEditCategory(CategoryViewModel categoryViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _quizService.CreateUpdateCategory(categoryViewModel, _userLogin);
                    return Redirect("/category");
                }

                ViewData["Title"] = "Create New Category";
                return View(categoryViewModel);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        public JsonResult DeleteCategory(int id)
        {
            try
            {
                return _quizService.DeleteCategory(id);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return new JsonResult("Error");
            }
        }
    }
}
