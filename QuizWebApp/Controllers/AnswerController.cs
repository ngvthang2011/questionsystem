﻿using Microsoft.AspNetCore.Mvc;
using QuizWebApp.Providers;
using QuizWebApp.Services;
using QuizWebApp.ViewModels;

namespace QuizWebApp.Controllers
{
    public class AnswerController : BaseController
    {
        private readonly ILoggerManager _loggerManager;
        private readonly QuizService _quizService;
        public AnswerController(ILoggerManager loggerManager, IQuizService quizService)
        {
            _loggerManager = loggerManager;
            _quizService = (QuizService)quizService;
        }

        public IActionResult Index()
        {
            try
            {
                var answerList = _quizService.GetAnswerList();
                return View(answerList);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        public IActionResult CreateEditAnswer(long? id)
        {
            try
            {
                AnswerViewModel answer;
                ViewData["Questions"] = _quizService.GetQuestionList();

                if (id != null && id != 0)
                {
                    ViewData["Title"] = "Update Answer";
                    answer = _quizService!.GetAnswerById((long)id)!;
                }
                else
                {
                    ViewData["Title"] = "Create New Answer";
                    answer = new AnswerViewModel();
                }

                return View(answer);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult CreateEditAnswer(AnswerViewModel answerViewModel, IFormFile? Image)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _quizService.CreateUpdateAnswer(answerViewModel, _userLogin, Image);
                    return Redirect("/answer");
                }

                ViewData["Title"] = "Create New Answer";
                ViewData["Questions"] = _quizService.GetQuestionList();
                return View(answerViewModel);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        public JsonResult DeleteAnswer(int id)
        {
            try
            {
                return _quizService.DeleteAnswer(id);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return new JsonResult("Error");
            }
            
        }

        [HttpPost]
        public IActionResult ExportExcelAnswer()
        {
            try
            {
                var questionList = _quizService.GetAnswerList();

                byte[] file = questionList.ExportExcel<AnswerViewModel>();
                return File(file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "answer.xlsx");
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult ImportExcelAnswer(IFormFile ExcelAnswerFile)
        {
            try
            {
                if (ExcelAnswerFile != null)
                {
                    if (ExcelAnswerFile.ContentType == "application/vnd.ms-excel" || ExcelAnswerFile.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        _quizService.ImportExcelAnswer(ExcelAnswerFile, _userLogin);
                        return Redirect("/answer");
                    }
                    else
                    {
                        ModelState.AddModelError("File", "Unable to Upload file!");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Unable to Upload file!");
                    return View();
                }
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }
    }
}
