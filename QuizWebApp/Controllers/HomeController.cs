﻿using Microsoft.AspNetCore.Mvc;
using QuizWebApp.Models;
using QuizWebApp.Providers;
using QuizWebApp.Services;
using System.Diagnostics;

namespace QuizWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILoggerManager _loggerManager;
        private readonly QuizService _quizService;
        public HomeController(ILoggerManager logger, IQuizService quizService)
        {
            _loggerManager = logger;
            _quizService = (QuizService)quizService;
        }

        public IActionResult Index()
        {
            try
            {
                var categoryList = _quizService.GetCategoryList();
                return View(categoryList);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}