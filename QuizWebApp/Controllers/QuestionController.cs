﻿using Microsoft.AspNetCore.Mvc;
using QuizWebApp.Providers;
using QuizWebApp.Services;
using QuizWebApp.ViewModels;

namespace QuizWebApp.Controllers
{
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.

    public class QuestionController : BaseController
    {
        private readonly ILoggerManager _loggerManager;
        private readonly QuizService _quizService;
        public QuestionController(ILoggerManager logger, IQuizService quizService)
        {
            _loggerManager = logger;
            _quizService = (QuizService)quizService;
        }

        public IActionResult Index()
        {
            try
            {
                var questionList = _quizService.GetQuestionList();
                return View(questionList);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        public IActionResult CreateEditQuestion(long? id)
        {
            try
            {
                QuestionViewModel question;
                ViewData["Categories"] = _quizService.GetCategoryList();

                if (id != null && id != 0)
                {
                    ViewData["Title"] = "Update Question";
                    question = _quizService.GetQuestionById((long)id);
                }
                else
                {
                    ViewData["Title"] = "Create New Question";
                    question = new QuestionViewModel();
                }

                return View(question);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult CreateEditQuestion(QuestionViewModel questionViewModel, IFormFile? Image)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _quizService.CreateUpdateQuestion(questionViewModel, _userLogin, Image);
                    return Redirect("/question");
                }

                ViewData["Title"] = "Create New Question";
                ViewData["Categories"] = _quizService.GetCategoryList();
                return View(questionViewModel);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        public JsonResult DeleteQuestion(int id)
        {
            try
            {
                return _quizService.DeleteQuestion(id);
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return new JsonResult("Error");
            }
        }

        [HttpPost]
        public IActionResult ExportExcelQuestion()
        {
            try
            {
                var questionList = _quizService.GetQuestionList();
                byte[] file = questionList.ExportExcel<QuestionViewModel>();

                return File(file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "question.xlsx");
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult ImportExcelQuestion(IFormFile ExcelQuestionFile)
        {
            try
            {
                if (ExcelQuestionFile != null)
                {
                    if (ExcelQuestionFile.ContentType == "application/vnd.ms-excel" || ExcelQuestionFile.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        _quizService.ImportExcelQuestion(ExcelQuestionFile, _userLogin);
                        return Redirect("/question");
                    }
                    else
                    {
                        ModelState.AddModelError("File", "Unable to Upload file!");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Unable to Upload file!");
                    return View();
                }
            }
            catch (Exception ex)
            {
                _loggerManager.LogError(ex.ToString());
                return NotFound();
            }
        }

    }
}
